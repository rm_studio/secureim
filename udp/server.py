#!/usr/bin/python

import socket
import os
import fcntl, struct
import threading, time
import re

# const variable
SERVER_PORT = 52216
BUFFER_SIZE = 8092

# keep udp client connection time.
BEAT_PERIOD = 5
BEAT_COMMAND = r'HEARTBEAT'
APPLY_COMMAND = r'^APPLY\s?(\d+)L\s?,\s?(\d+)\s?$'

# global variable
# a set of connected host.
conn_host = set()


def heartbeat( udp ):
    # use the udp object already created.
    # Heartbeat start...

    while True:
        global conn_host
        for host in conn_host:
            try:
                # send heartbeat message.
                udp.sendto( BEAT_COMMAND, host ) 
            except socket.error as e:
                # remove host from connected host list.
                # but this may not work. orz..
                conn_host.remove( host )
                print 'Host %s may lose connection. Msg: %s' % ( str(host), e )

        # wait serval seconds.
        time.sleep( BEAT_PERIOD )


def receive( udp ):
    print 'Waiting for clients...'

    # wait for connections.
    while True:
        # receive data from connections.
        cmd, addr = udp.recvfrom( BUFFER_SIZE )

        if cmd:
            # no stable udp-connection adding here.
            # next step: use icmp for detecting udp status.
            global conn_host
            conn_host.add( addr )

            print 'Received by', addr
            print 'Data:', cmd

            # paste command
            if cmd == 'list':
                # do not sent itself to requestor
                conn_list = conn_host.copy()
                conn_list.remove( addr )

                # send connected client list.
                udp.sendto( 'LIST '+str(list(conn_list)), addr )

            if re.match(APPLY_COMMAND, cmd.upper()):
                command = re.search(APPLY_COMMAND, cmd.upper())
                # parse ip address.
                ip = socket.inet_ntoa(
                    struct.pack( "i", 
                        socket.htonl( int( command.group(1) ) )))
                port = int( command.group(2) )
                
                # send to whom
                to_addr = ( ip, port )

                # transformed original ip
                t_ip = socket.ntohl(struct.unpack("i",socket.inet_aton( addr[0] ))[0])

                # send block hold apply to clients.
                udp.sendto( 'APPLY %sL,%s IN %d' % (t_ip, addr[1], port), to_addr )

                print to_addr

            if cmd == 'q':
                conn_host.remove( addr )

            # socket.ntohl(struct.unpack("i",socket.inet_aton("10.10.58.64"))[0])
            # socket.inet_ntoa(struct.pack("i", socket.htonl(168442432)))


def get_public_ip( ifname = 'eth0' ):
    # get public network ip automatic.
    try:  
        # get ip from socket
        s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )    
        return socket.inet_ntoa( fcntl.ioctl(
            s.fileno(),
            0x8915, # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24] )
    except:  
        # get ip from unix cmd-line
        ips = os.popen(
            "LANG=C ifconfig | grep \"inet addr\" | grep -v \"127.0.0.1\" | awk -F \":\" '{print $2}' | awk '{print $1}'").readlines()  
        if len(ips) > 0:  
            return ips[0]  

    # get local ip, etc. 192.168.1.10 / 10.0.1.25
    return socket.gethostbyname( socket.gethostname() ) 


if __name__ == '__main__':
    # get public network ip automatic.
    public_ip = get_public_ip('eth1')

    # create socket object.
    udp = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
    print 'Server IP:', public_ip

    # Reuse the address or you must waitfor serval minutes when you close this process.
    # NAT traversal must reuse the port.
    udp.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

    # bind port to local ip address.
    udp.bind( (public_ip, SERVER_PORT) )

    # create heartbeat connection thread
    thread_heartbeat = threading.Thread( target = heartbeat, args = (udp,) )
    # create receive thread
    thread_receive = threading.Thread( target = receive, args = (udp,) )

    threads = [thread_heartbeat, thread_receive]

    # main thread.
    for t in threads:
        t.setDaemon(True)
        t.start()

    # wait in main thread.
    for t in threads: t.join()
