#!/usr/bin/python

import socket, struct
import threading, time
import sys
import signal
import re

# const variable
HOST = '192.168.1.102'	#server ip
PORT = 52216	    	#server port
SERVER = (HOST, PORT)

BUFFER_SIZE = 8092
BEAT_COMMAND = 'HEARTBEAT'
APPLY_COMMAND = r'^APPLY\s?(\d+)L\s?,\s?(\d+)\s?IN\s?(\d+)\s?$'
SEND_COMMAND = r'^SEND\s?(\d+)L\s?,\s?(\d+)\s?WITH\s?(.+)$'

 
# signal handler
def sigint_handler(signum, frame):
    global is_sigint_up
    is_sigint_up = True
    print 'catched interrupt signal!'


def receive( udp ):
    # wait for receive data immediately

    while True:
        data, addr = udp.recvfrom(BUFFER_SIZE)

        if data != BEAT_COMMAND:
            #print '%s:%s> %s' % ( addr[0], addr[1], data )
            print '\n%s' % data
            sys.stdout.flush()

        if re.match(APPLY_COMMAND, data.upper()):
            command = re.search(APPLY_COMMAND, data.upper())
            # parse ip address.
            ip = socket.inet_ntoa(
                struct.pack( "i", 
                    socket.htonl( int( command.group(1) ) )))
            port = int( command.group(2) )
            orig_port = int( command.group(3) )
            to_addr = ( ip, port )

            p2p_udp = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
            p2p_udp.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
            p2p_udp.bind( (socket.gethostbyname( socket.gethostname() ), orig_port ) )

            p2p_udp.sendto( 'HOLE', to_addr )

            data, addr = p2p_udp.recvfrom(BUFFER_SIZE)
            print data, addr


# main
if __name__ == '__main__':
    # create socket object.
    udp = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    udp.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

    # connect to server
    udp.connect( SERVER )
    udp.send('connect')
    print 'Connected to %s:%d' % SERVER

    # create receive thread
    thread_receive = threading.Thread( target = receive, args = (udp,) )
    thread_receive.setDaemon( True )
    thread_receive.start()

    # when programme is halt, send command to server.
    # but it's not so good method.
    signal.signal(signal.SIGINT, sigint_handler)
    signal.signal(signal.SIGHUP, sigint_handler)
    signal.signal(signal.SIGTERM, sigint_handler)
    is_sigint_up = False

    command = None
    session = SERVER
    while command != 'q':
        try:
            # send some data to server
            command = raw_input( '%s:%s> ' % session )
            udp.send( command )

            if re.match(SEND_COMMAND, command.upper()):
                command = re.search(SEND_COMMAND, command.upper())

                # parse ip address.
                ip = socket.inet_ntoa(
                    struct.pack( "i", 
                        socket.htonl( int( command.group(1) ) )))
                port = int( command.group(2) )
                message = command.group(3)
                addr = (ip, port)

                p2p_udp = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
                p2p_udp.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
                p2p_udp.sendto( message, (ip, port) ) 

            if is_sigint_up:
                udp.sendto( 'q', session )
                break

        except Exception, e:
            udp.sendto( 'q', session )
            break
