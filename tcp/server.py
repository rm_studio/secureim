#!/usr/bin/python

import socket
import os
import fcntl, struct

# const
SERVER_PORT = 52216
BUFFER_SIZE = 1024

def get_public_ip( ifname = 'eth0' ):
    try:  
        # get ip from socket
        s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )    
        return socket.inet_ntoa( fcntl.ioctl(
            s.fileno(),
            0x8915, # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24] )
    except:  
        # get ip from unix cmd-line
        ips = os.popen(
            "LANG=C ifconfig | grep \"inet addr\" | grep -v \"127.0.0.1\" | awk -F \":\" '{print $2}' | awk '{print $1}'").readlines()  
        if len(ips) > 0:  
            return ips[0]  

    # get local ip, etc. 192.168.1.10 / 10.0.1.25
    return socket.gethostbyname( socket.gethostname() ) 


if __name__ == '__main__':
    public_ip = get_public_ip('eth1')

    # create socket object.
    tcp = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
    print 'Server IP:', public_ip
    print 'Waiting for clients...'

    # Reuse the address or you must waitfor serval minutes when you close this process.
    # NAT traversal must reuse the port.
    tcp.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

    # bind port to local ip address.
    tcp.bind( (public_ip, SERVER_PORT) )

    # listen port.
    tcp.listen( 5 )

    # wait for connections.
    while True:
        conn, addr = tcp.accept()
        print 'Connected by', addr
        while True:
            data = conn.recv( BUFFER_SIZE )
            print '%s: %s' % ( addr[0], data )

            # data feedback.
            conn.send( 'You say "%s".' % data )
