#!/usr/bin/python

import socket

# const variable
HOST = '120.24.90.148'	#server ip
PORT = 52216	    	#server port
SERVER = (HOST, PORT)

# main
if __name__ == '__main__':
    # create socket object.
    tcp = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    tcp.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

    # connect to server
    tcp.connect( SERVER )
    print 'Connected to %s:%d' % SERVER

    # send some data to server
    data = 'Hello, world!' 
    tcp.send(data)
    print 'localhost:', data

    # wait for receive data immediately
    while True:
        data = tcp.recv(1024)
        print data
